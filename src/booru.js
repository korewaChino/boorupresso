const fetch = require('fetch-json')
class Booru {

    static async random(tags, url){
        switch(url){
            case "danbooru":
                url = `https://danbooru.donmai.us/posts/random.json`;
                break;
            case "gelbooru":
                url = `https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&limit=1`;
                break;
            case 'safebooru':
                url = `https://safebooru.org/index.php?page=dapi&s=post&q=index&json=1&limit=1`;
                break;
        }
        console.log(`Fetching JSON post... [${tags}]`)
        let tags2=tags.replace(' ', '+')

        var data = await fetch.get(url, {"limit": 1, "random": true, "tags": tags2})
        return data;
    }
}
module.exports = Booru;